const arrKetQuaThemSo = [];
//Thêm số
themSo("#btnThemSo", "#nhapSo", "#myForm", arrKetQuaThemSo, "#ketQuaThemSo");
//xóa số
xoaSo("#btnXoaSo", arrKetQuaThemSo, "#ketQuaThemSo");

//----------------------- Bài 1--------------------------
document.querySelector("#btnTinhTong1").onclick = function () {
  var ketQua1 = 0;
  for (var i = 0; i < arrKetQuaThemSo.length; i++) {
    if (arrKetQuaThemSo[i] >= 0) {
      ketQua1 = ketQua1 + arrKetQuaThemSo[i];
    }
  }
  document.querySelector("#ketQuaTongSoDuong1").innerHTML =
    ` <i class="fa fa-hand-holding" style="font-size: 30px; color: orange"> </i> Tổng là : ` +
    ketQua1;
};
//==================== Bài 2=================================
document.querySelector("#btnDemSo2").onclick = function () {
  var dem = demSoDuong(arrKetQuaThemSo);
  document.querySelector("#ketQuaDemSo2").innerHTML =
    ` <i class="fa fa-hand-holding" style="font-size: 30px; color: orange"> </i> Có tổng cộng là : ` +
    dem +
    " số dương";
};
//Bài 3
document.querySelector("#btnTimSo3").onclick = function () {
  var ketQua3 = soBeNhat(arrKetQuaThemSo);
  document.querySelector("#ketQuaTimSo3").innerHTML =
    ` <i class="fa fa-hand-holding" style="font-size: 30px; color: orange"> </i> số bé nhất là : ` +
    ketQua3;
};
//========================= Bài 4===============================
document.querySelector("#btnTimSo4").onclick = function () {
  // Cách 1
  // var arrKetQuaThemSo4 = arrKetQuaThemSo.slice(0);
  // var sapXep4 = arrKetQuaThemSo4.sort((a, b) => a - b);
  // var ketQua4 = 0;
  // for (var i = 0; i < sapXep4.length; i++) {
  //   if (sapXep4[i] >= 0) {
  //     ketQua4 = sapXep4[i];
  //     break;
  //   }
  // }
  // document.querySelector("#ketQuaTimSo4").innerHTML = ketQua4;
  //cách 2
  var ketQua4 = 0;
  for (a = 0; a < arrKetQuaThemSo.length; a++) {
    if (arrKetQuaThemSo[a] > 0) {
      ketQua4 = arrKetQuaThemSo[a];
      break;
    }
  }
  for (var i = 0; i < arrKetQuaThemSo.length; i++) {
    if (arrKetQuaThemSo[i] > 0) {
      if (ketQua4 <= arrKetQuaThemSo[i]) {
        ketQua4 = ketQua4;
      } else {
        ketQua4 = arrKetQuaThemSo[i];
      }
    }
  }
  document.querySelector("#ketQuaTimSo4").innerHTML =
    ` <i class="fa fa-hand-holding" style="font-size: 30px; color: orange"> </i> số dương bé nhất là : ` +
    ketQua4;
};

//==============================Bài 5=======================
document.querySelector("#btnTimSo5").onclick = function () {
  var ketQua5 = 0;
  for (var i = 1; i <= arrKetQuaThemSo.length; i++) {
    var viTriCuoi = arrKetQuaThemSo.length - i;
    if (arrKetQuaThemSo[viTriCuoi] % 2 == 0) {
      ketQua5 = arrKetQuaThemSo[viTriCuoi];
      break;
    } else {
      ketQua5 = -1;
    }
  }
  document.querySelector("#ketQuaTimSo5").innerHTML =
    ` <i class="fa fa-hand-holding" style="font-size: 30px; color: orange"> </i> số chẵn cuối là : ` +
    ketQua5;
};
//===========================Bài 6:====================

function doiCho6() {
  var nhapViTri1 = document.querySelector("#nhapViTri1").value * 1;
  var nhapViTri2 = document.querySelector("#nhapViTri2").value * 1;
  var arrKetQuaThemSo6 = arrKetQuaThemSo.slice(0);
  var trungGian = arrKetQuaThemSo6.slice(nhapViTri1, nhapViTri1 + 1);
  arrKetQuaThemSo6[nhapViTri1] = arrKetQuaThemSo6[nhapViTri2];
  arrKetQuaThemSo6[nhapViTri2] = trungGian;

  document.querySelector("#ketQuaDoiCho6").innerHTML =
    ` <i class="fa fa-hand-holding" style="font-size: 30px; color: orange"> </i> kết quả sau khi đổi chỗ là : ` +
    arrKetQuaThemSo6;
}

//===========================Bài 7==========================
document.querySelector("#btnSapXep7").onclick = function () {
  var arrKetQuaThemSo7 = arrKetQuaThemSo.slice(0);

  /*Cách 1
  var ketQua7 = [];
  for (var i = 0; i < arrKetQuaThemSo.length; i++) {
    var soNhoNhat = soBeNhat(arrKetQuaThemSo7);
    var viTriSoNhoNhat = arrKetQuaThemSo7.indexOf(soNhoNhat);
    var laySoNhoNhat = arrKetQuaThemSo7.splice(viTriSoNhoNhat, 1);
    ketQua7.push(laySoNhoNhat);
  }
*/
  //  Cách 2
  arrKetQuaThemSo7.sort(function (a, b) {
    return a - b;
  });
  ketQua7 = arrKetQuaThemSo7;

  document.querySelector("#ketQuaSapXep7").innerHTML =
    ` <i class="fa fa-hand-holding" style="font-size: 30px; color: orange"> </i> Dãy số sau khi được sắp xếp là : ` +
    ketQua7;
};

//===========================Bài 8======================
document.querySelector("#btnTimSo8").onclick = function () {
  for (var i = 0; i < arrKetQuaThemSo.length; i++) {
    if (arrKetQuaThemSo[i] == soNguyenTo(arrKetQuaThemSo[i])) {
      var ketQua8 = arrKetQuaThemSo[i];
      break;
    } else {
      var ketQua8 = -1;
    }
  }
  document.querySelector("#ketQuaTimSo8").innerHTML =
    ` <i class="fa fa-hand-holding" style="font-size: 30px; color: orange"> </i> Số nguyên tố đầu tiên là là : ` +
    ketQua8;
};

//========================Bài 9==========================
document.querySelector("#btnDemSoNguyen9").onclick = function () {
  var dem = 0;
  for (var i = 0; i < arrKetQuaThemSo.length; i++) {
    if (Number.isInteger(arrKetQuaThemSo[i])) {
      dem++;
    }
  }
  document.querySelector("#ketQuaDemSoNguyen9").innerHTML =
    ` <i class="fa fa-hand-holding" style="font-size: 30px; color: orange"> </i>  có tổng cộng : ` +
    dem +
    " số nguyên";
};

//========================Bài 10=======================
document.querySelector("#btnSoSanh10").onclick = function () {
  var tongSoDuong = demSoDuong(arrKetQuaThemSo);
  var tongSoAm = demSoAm(arrKetQuaThemSo);
  var ketQua10 = "";
  if (tongSoDuong > tongSoAm) {
    ketQua10 = "Số dương nhiều hơn số âm";
  } else if (tongSoDuong < tongSoAm) {
    ketQua10 = "Số âm nhiều hơn số dương";
  } else {
    ketQua10 = "Số âm và số dương bằng nhau";
  }

  document.querySelector("#ketQuaSoSanh10").innerHTML =
    ` <i class="fa fa-hand-holding" style="font-size: 30px; color: orange"> </i> ` +
    ketQua10;
};
