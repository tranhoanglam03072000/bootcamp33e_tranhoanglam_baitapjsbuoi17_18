// Thêm Số vào 1 mảng
function themSo(idBtn, idInput, idform, nameArr, idKetQua) {
  document.querySelector(idBtn).onclick = function () {
    event.preventDefault();
    var nhapSo = document.querySelector(idInput).value;
    if (nhapSo == "") {
      return;
    }
    document.querySelector(idform).reset();
    nameArr.push(nhapSo * 1);
    document.querySelector(idKetQua).innerHTML = nameArr;
  };
}
// Xóa số khỏi 1 mảng
function xoaSo(idBtn, nameArr, idKetQua) {
  document.querySelector(idBtn).onclick = function () {
    nameArr.pop();
    document.querySelector(idKetQua).innerHTML = nameArr;
  };
}
//tìm số bé nhất trong mảng
function soBeNhat(arr) {
  var ketQua = arr[0];
  for (var i = 0; i < arr.length; i++) {
    if (ketQua > arr[i]) {
      ketQua = arr[i];
    }
  }
  return ketQua;
}
//Kiểm tra số đó có phải SNT k
function soNguyenTo(sNT) {
  var dem = 0;
  for (var i = 1; i <= sNT; i++) {
    if (sNT % i == 0) {
      dem++;
    }
  }
  if (dem == 2) {
    var ketQua = sNT;
  }
  return ketQua;
}

// Đếm số dương trong mảng
function demSoDuong(arr) {
  var dem = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      dem++;
    }
  }
  return dem;
}
//Đếm số âm trong mảng
function demSoAm(arr) {
  var dem = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] < 0) {
      dem++;
    }
  }
  return dem;
}
